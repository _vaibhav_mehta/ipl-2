//matchIDs per year
const years = (matches, year) => {
  const matchIDs = matches
    .filter(match => match.season === year.toString())
    .reduce((matchesYear, match) => {
      matchesYear[match.id] = 1;
      return matchesYear;
    }, {});
  return matchIDs;
};

//task -1
const matchesPlayed = matches => {
  
  return matches.reduce((totalMatchesPlayed, match) => {
    totalMatchesPlayed[match.season] =
      (totalMatchesPlayed[match.season] || 0) + 1;
    return totalMatchesPlayed;
  }, {});
};
//task 2
const matcheswon = matches => {
  return matches.reduce((matchesWonTeamYear, match) => {
    if (matchesWonTeamYear[match.season] === undefined) {
      matchesWonTeamYear[match.season] = {};
    }
    matchesWonTeamYear[match.season][match.winner] =
      (matchesWonTeamYear[match.season][match.winner] || 0) + 1;
    return matchesWonTeamYear;
  }, {});
};
//task -3
const extra = (deliveries, matches) => {
  //Extra runs conceded by each team
  //in the year 2016 in IPL
  const matchIDsYear = years(matches, 2016);
  return deliveries
    .filter(delivery => delivery.match_id in matchIDsYear)
    .reduce((extraRunsTeam, delivery) => {
      extraRunsTeam[delivery.bowling_team] =
        (extraRunsTeam[delivery.bowling_team] || 0) +
        parseInt(delivery.extra_runs);
      return extraRunsTeam;
    }, {});
};
//   let extraRuns = {};
//   deliveries.filter(delivery => {
//     if (ID.indexOf(delivery.match_id) !== -1) {
//       if (extraRuns[delivery.bowling_team]) {
//         extraRuns[delivery.bowling_team] += parseInt(delivery.extra_runs, 10);
//       } else {
//         extraRuns[delivery.bowling_team] = Number(delivery.extra_runs);
//       }
//     }
//   });
//   return extraRuns;
//};


//task-4
const economy = (matches, deliveries) => {
  const matchIDsYear = years(matches, 2015);
  const bowlerRunsOverYear = deliveries
    .filter(delivery => delivery.match_id in matchIDsYear)
    .reduce((bowlerRunsOvers, delivery) => {
      if (bowlerRunsOvers[delivery.bowler] === undefined) {
        bowlerRunsOvers[delivery.bowler] = {};
      }
      bowlerRunsOvers[delivery.bowler].runs =
        (bowlerRunsOvers[delivery.bowler].runs || 0) +
        parseInt(delivery.total_runs) -
        parseInt(delivery.bye_runs) -
        parseInt(delivery.legbye_runs);
      if (!parseInt(delivery.wide_runs) && !parseInt(delivery.noball_runs)) {
        bowlerRunsOvers[delivery.bowler].overs =
          (bowlerRunsOvers[delivery.bowler].overs || 0) + 1 / 6;
      }
      return bowlerRunsOvers;
    }, {});
  const bowlerEcoYear = Object.entries(bowlerRunsOverYear).reduce(
    (bowlerEco, bowler) => {
      if (bowler[1].overs >= 2) {
        bowlerEco[bowler[0]] = bowler[1].runs / bowler[1].overs;
      }
      return bowlerEco;
    },
    {}
  );
  return Object.entries(bowlerEcoYear)
    .sort((a, b) => a[1] - b[1])
    .slice(0, 10)
    .reduce((topEcoBowler, bowler) => {
      topEcoBowler[bowler[0]] = parseFloat(bowler[1].toFixed(2));
      return topEcoBowler;
    }, {});
};


//   let id2015 = {};
//   matches.forEach(match => {
//     if (match.season === "2015") {
//       id2015[match.id] = 1;
//     }
//   });

//   const bowlerRunOver = {},
//     bowlerEconomy = {},
//     top10 = {};
//   deliveries
//     .filter(delivery => delivery.match_id in id2015)
//     .forEach(delivery => {
//       if (bowlerRunOver[delivery.bowler] === undefined) {
//         bowlerRunOver[delivery.bowler] = {};
//       }
//       bowlerRunOver[delivery.bowler].runs =
//         (bowlerRunOver[delivery.bowler].runs || 0) +
//         parseInt(delivery.total_runs) -
//         parseInt(delivery.bye_runs) -
//         parseInt(delivery.legbye_runs);
//       if (!parseInt(delivery.wide_runs) && !parseInt(delivery.noball_runs)) {
//         bowlerRunOver[delivery.bowler].overs =
//           (bowlerRunOver[delivery.bowler].overs || 0) + 1 / 6;
//       }
//     });
//   for (let bowler in bowlerRunOver) {
//     if (bowlerRunOver[bowler].overs >= 2) {
//       bowlerEconomy[bowler] =
//         bowlerRunOver[bowler].runs / bowlerRunOver[bowler].overs;
//     }
//   }
//   Object.entries(bowlerEconomy)
//     .sort((a, b) => a[1] - b[1])
//     .slice(0, 10)
//     .forEach(arr => {
//       top10[arr[0]] = parseFloat(arr[1].toFixed(2));
//     });
//   return top10;


//function exports
module.exports = {
  matchesPlayed,
  matcheswon,
  extra,
  economy
};
