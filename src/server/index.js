//function imports
const { matchesPlayed, matcheswon, extra, economy } = require("./ipl");

const CSVToJSON = require("csvtojson");
//function to print the object to the json file
const writeToJson = (obj, location) => {
  var fs = require("fs");
  var json = JSON.stringify(obj);
  fs.writeFile(location, json, function(err) {
    if (err) throw err;
    console.log("writing sucessfull");
  });
};

// task-1
CSVToJSON()
  .fromFile("./data/matches.csv")
  .then(matches => {
    //console.log(matchesPlayed(matches));
    //function call
    const matchesPlayedRes = matchesPlayed(matches);
    //Writing the output in the file
    writeToJson(matchesPlayedRes, "./output/matchesPlayedPerYear.json");

    // task -2
    //calling the function
    //console.log(matcheswon(matches));
    const matchesWonRes = matcheswon(matches);
    // copying the contents inside the new json file
    writeToJson(matchesWonRes, "./output/matchesWonOfPerTeamPerYear.json");

    //task-3
    CSVToJSON()
      .fromFile("./data/deliveries.csv")
      .then(deliveries => {
        //function call
        //console.log (extra(ID,deliveries));
        const extraRes = extra(deliveries, matches);
        writeToJson(extraRes, "./output/extraRunIn2016.json");

        //task 4
        // Returns the 10 most economical bowlers
        // in the year 2015 in IPL

        //function call
        const economyRes = economy(deliveries, matches);
        // console.log(output4);
        //writing it inside a new json file
        writeToJson(economyRes, "./output/top10ecnomybowler.json");
      });
  });
