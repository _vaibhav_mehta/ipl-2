

var http = require('http');
var fs = require('fs');

var server = http.createServer(function (req, resp) {
    console.log(req.url);
    if (req.url === "/create") {
        fs.readFile("/Users/vaibhavmehta/Desktop/drills/ipl-2/src/client/index.html", function (error, pgResp) {
            if (error) {
                resp.writeHead(404);
                resp.write('Contents you are looking are Not Found');
            } else {
                resp.writeHead(200, { 'Content-Type': 'text/html' });
                resp.write(pgResp);
            }
             
            resp.end();
        });
    } else if(req.url === "/app.css"){
       
        fs.readFile("/Users/vaibhavmehta/Desktop/drills/ipl-2/src/client/app.css", function (error, pgResp) {
            if (error) {
                resp.writeHead(404);
                resp.write('Contents you are looking are Not Found');
            } else {
                resp.writeHead(200, { 'Content-Type': 'text/json' });
                // resp.write(pgResp);
            }
             
            resp.end(pgResp);
        });
    }
    else if(req.url ==='/extraRunIn2016.json')
    {
        fs.readFile("/Users/vaibhavmehta/Desktop/drills/ipl-2/src/output/extraRunIn2016.json", function (error, pgResp) {
            if (error) {
                resp.writeHead(404);
                resp.write('Contents you are looking are Not Found');
            } else {
                resp.writeHead(200, { 'Content-Type': 'text/json' });
                // resp.write(pgResp);
            }
             
            resp.end(pgResp);
        });
    }
    else if(req.url==='/matchesPlayedPerYear.json')
    {
        fs.readFile("/Users/vaibhavmehta/Desktop/drills/ipl-2/src/output/matchesPlayedPerYear.json", function (error, pgResp) {
            if (error) {
                resp.writeHead(404);
                resp.write('Contents you are looking are Not Found');
            } else {
                resp.writeHead(200, { 'Content-Type': 'text/json' });
                // resp.write(pgResp);
            }
             
            resp.end(pgResp);
        });
    }
    else if(req.url==='/matchesWonOfPerTeamPerYear.json')
    {
        fs.readFile("/Users/vaibhavmehta/Desktop/drills/ipl-2/src/output/matchesWonOfPerTeamPerYear.json", function (error, pgResp) {
            if (error) {
                resp.writeHead(404);
                resp.write('Contents you are looking are Not Found');
            } else {
                resp.writeHead(200, { 'Content-Type': 'text/json' });
                // resp.write(pgResp);
            }
             
            resp.end(pgResp);
        });
    }
    else if(req.url === '/top10economybowler.json')
    {
        fs.readFile("/Users/vaibhavmehta/Desktop/drills/ipl-2/src/output/top10ecnomybowler.json", function (error, pgResp) {
            if (error) {
                resp.writeHead(404);
                resp.write('Contents you are looking are Not Found');
            } else {
                resp.writeHead(200, { 'Content-Type': 'text/json' });
                // resp.write(pgResp);
            }
             
            resp.end(pgResp);
        });
    }
    else if(req.url ==='/app.js')
    {
        fs.readFile("/Users/vaibhavmehta/Desktop/drills/ipl-2/src/client/app.js", function (error, pgResp) {
            if (error) {
                resp.writeHead(404);
                resp.write('Contents you are looking are Not Found');
            } else {
                resp.writeHead(200, { 'Content-Type': 'text/javascript' });
                // resp.write(pgResp);
            }
             
            resp.end(pgResp);
        });
    }
});

server.listen(5050);
 
console.log('Server Started listening on 5050');