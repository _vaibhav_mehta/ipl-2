   
         
        const makechart = (data,data1,container,titleText,xAxisTitile) =>{
            Highcharts.chart(container, {
                chart: {
                  type: 'column'
                },
                title: {
                  text: titleText
                }
                // subtitle: {
                //   text: 'Source: WorldClimate.com'
                // }
                ,
                xAxis: {
                  categories: data,
                  crosshair: true
                },
                yAxis: {
                  min: 0,
                  title: {
                    text: 'Range'
                  }
                },
                tooltip: {
                  headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                  pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                    '<td style="padding:0"><b>{point.y:.1f} mm</b></td></tr>',
                  footerFormat: '</table>',
                  shared: true,
                  useHTML: true
                },
                plotOptions: {
                  column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                  }
                },
                series: [{
                  name: xAxisTitile,
                  data: data1
              
                }]
              });
        }
        //task-1
        let matchesWonOfPerTeamPerYear=new Request("http://localhost:5050/matchesPlayedPerYear.json");
        fetch(matchesWonOfPerTeamPerYear)
            .then(function(resp){
                return resp.json();
            })
            .then(function (data){
                console.log(data);
                // Object.keys(data).forEach(item=> {
                //     console.log(data[item])
                // }) 
            makechart (Object.keys(data),Object.values(data),'container',"Matches Won Per Year Per Team","No. of Matches Played");      
        })

        //task-2
        let extraRuns2016=new Request("http://localhost:5050/extraRunIn2016.json");
        fetch(extraRuns2016)
            .then(function(resp){
                return resp.json();
            })
            .then(function (data){
                console.log(data);
                // Object.keys(data).forEach(item=> {
                //     console.log(data[item])
                // }) 
            makechart (Object.keys(data),Object.values(data),'container2',"Extra Runs Conceded per team in 2016","Teams");      
        })
        //task-3
        let top10EcoBowler=new Request("http://localhost:5050/top10economybowler.json");
        fetch(top10EcoBowler)
            .then(function(resp){
                return resp.json();
            })
            .then(function (data){
                console.log(data);
      
             makechart (Object.keys(data),Object.values(data),'container3',"Top 10 economical Bowlers","Bowler names");      
        })
        //task-4
        let matchesWonPerTeamPerYear=new Request("http://localhost:5050/matchesWonOfPerTeamPerYear.json");
        var final=[];
        fetch(matchesWonPerTeamPerYear)
            .then(function(resp){
                return resp.json();
            })
            .then(function (data){
               //console.log(data);
            
            let initial = Object.entries(data);
            //let next = Object.entries(initial[0][1]);
            console.log(initial);
            
            // console.log(Object.keys(obj));
            let kkr = initial.reduce((winsPerYearKKR, ele) => {
                winsPerYearKKR[ele[0]] = ele[1]['Kolkata Knight Riders'];
                return winsPerYearKKR;
              }, {});
              console.log(kkr);

              let pune = initial.reduce((winsPerYearPune, ele) => {
                winsPerYearPune[ele[0]] = (winsPerYearPune[ele[0]] || 0) + (ele[1]['Pune Warriors'] || 0) + (ele[1]['Rising Pune Supergiant'] || 0) + (ele[1]['Rising Pune Supergiants'] || 0);
                return winsPerYearPune;
              }, {});
              //console.log(pune);

            
            let rcb = initial.reduce((winsPerYearRCB, ele) => {
                winsPerYearRCB[ele[0]] = ele[1]['Royal Challengers Bangalore'];
                return winsPerYearRCB;
            }, {});
             //console.log(rcb);
            
             let mi = initial.reduce((winsPerYearMI, ele) => {
                winsPerYearMI[ele[0]] = ele[1]['Mumbai Indians'];
                return winsPerYearMI;
            }, {});
           // console.log(mi);
            let kxip = initial.reduce((winsPerYearKXIP, ele) => {
                winsPerYearKXIP[ele[0]] = ele[1]['Kings XI Punjab'];
                return winsPerYearKXIP;
            }, {});
            //console.log(kxip);
            let dd = initial.reduce((winsPerYearDD, ele) => {
                winsPerYearDD[ele[0]] = ele[1]['Delhi Daredevils'];
                return winsPerYearDD;
            }, {});
             //console.log(dd);
            let dc = initial.reduce((winsPerYearDC, ele) => {
                winsPerYearDC[ele[0]] = (winsPerYearDC[ele[0]] || 0) + (ele[1]['Deccan Chargers'] || 0) + (ele[1]['Sunrisers Hyderabad'] || 0);
                return winsPerYearDC;
            }, {});
            //console.log(dc);
            let rr = initial.reduce((winsPerYearRR, ele) => {
                winsPerYearRR[ele[0]] = (ele[1]['Rajasthan Royals'] || 0);
                return winsPerYearRR;
            }, {});
            
            //console.log(rr);
            let csk = initial.reduce((winsPerYearCSK, ele) => {
                winsPerYearCSK[ele[0]] = (ele[1]['Chennai Super Kings'] || 0);
                return winsPerYearCSK;
            }, {});
            //console.log(csk);
            let gl = initial.reduce((winsPerYearGL, ele) => {
                winsPerYearGL[ele[0]] = (ele[1]['Gujarat Lions'] || 0);
                return winsPerYearGL;
            }, {});
            //console.log(gl);

            let kt = initial.reduce((winsPerYearKT, ele) => {
                winsPerYearKT[ele[0]] = (ele[1]['Kochi Tuskers Kerala'] || 0);
                return winsPerYearKT;
            }, {});
            //console.log(kt);
            seriesArray=[];
            seriesArray.push({name:'kolkata knight riders',data:Object.values(kkr)});
            seriesArray.push({name:'Royal Challengers Bangalore',data:Object.values(rcb)});
            seriesArray.push({name:'Pune Warriors',data:Object.values(pune)});
            seriesArray.push({name:'Mumbai Indians',data:Object.values(mi)});
            seriesArray.push({name:'Kings XI Punjab',data:Object.values(kxip)});
            seriesArray.push({name:'Deccan Chargers',data:Object.values(dc)});
            seriesArray.push({name:'Rajasthan Royals',data:Object.values(rr)});
            seriesArray.push({name:'Chennai Super Kings',data:Object.values(csk)});
            seriesArray.push({name:'Gujarat Lions',data:Object.values(gl)});
            seriesArray.push({name:'Kochi Tuskers Kerala',data:Object.values(kt)});
            console.log(seriesArray);
            
           
            
            
            
            //makechart (Object.keys(data),,'container4'); 
            Highcharts.chart('container4', {
                chart: {
                  type: 'column'
                },
                title: {
                  text: 'Matches Won Per Team Per Year'
                },
                xAxis: {
                  categories: Object.keys(data),
                  crosshair: true
                },
                yAxis: {
                  min: 0,
                  title: {
                    text: 'Range'
                  }
                },
                tooltip: {
                  headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                  pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                    '<td style="padding:0"><b>{point.y:.1f} mm</b></td></tr>',
                  footerFormat: '</table>',
                  shared: true,
                  useHTML: true
                },
                plotOptions: {
                  column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                  }
                },
                series: seriesArray
              });  
              
        })
        